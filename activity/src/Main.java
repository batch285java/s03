import java.util.*;


public class Main {
        public static void main(String[] args) {

            List<Integer> list = Arrays.asList(1, 2, 3, 4);
            Set<Integer> set = new HashSet<>(Arrays.asList(1, 2, 3, 4));
            Map<Integer, String> map = new HashMap<>();
            map.put(1, "one");
            map.put(2, "two");
            map.put(3, "three");
            map.put(4, "four");


            System.out.println("Please enter an integer to calculate its factorial: ");


            Scanner in = new Scanner(System.in);


            int num = 0;
            while (true) {
                try {
                    num = in.nextInt();
                    if (num < 0) {
                        System.out.println("Invalid input. Please enter a non-negative integer.");
                        continue;
                    }
                    break;
                } catch (InputMismatchException e) {
                    System.out.println("Invalid input. Please enter an integer.");
                    in.next();
                }
            }


            int answer = 1;
            int counter = 1;

            if (num == 0) {
                System.out.println("Factorial of 0 is 1");
            } else {
                while (counter <= num) {
                    answer *= counter;
                    counter++;
                }

                System.out.println("Factorial of " + num + " is " + answer);
            }
        }
}



