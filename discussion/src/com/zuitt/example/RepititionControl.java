package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class RepititionControl {
    public static void main(String[] args){


        //loops
            int x = 0;

            while(x<10){
                System.out.println("While loop Loop Number: " + x);
                x++;
            }

            //do-

        int y = 0;
            do{
                System.out.println("Print this");
                y++;
            }while(y>10);


        //for loop




//        for(int i=0;i<10;i++){
//            System.out.println(i);
//        }

        //For loop with arrays

        int[] intArray = {100,200,300,400,500};
        for(int i=0; i<intArray.length; i++){
            System.out.println(intArray[i]);
        }


        //for each loop with array

        /*
        * Syntax:
        * for(dataType itemName : arrayName){
        *  //code block
        * }
        *
        * */


        String[] boyBandArray ={"John","Paul","George","Ringo"};

        for(String member: boyBandArray){
            System.out.println(member);
        }


        //Multi dimensional array
        String[][] classroom = new String[3][3];
        //[row][column]

        //First row
        classroom[0][0] = "Jenny";
        classroom[0][1] = "Liza";
        classroom[0][2] = "Rose";
        //Second row
        classroom[1][0] = "Ash";
        classroom[1][1] = "Misty";
        classroom[1][2] = "Brock";
        //Third row
        classroom[2][0] = "Amy";
        classroom[2][1] = "Lulu";
        classroom[2][2] = "Morgan";

        for(int row = 0;row<3;row++){
            for(int col=0;col<3;col++){
                System.out.println("Classroom [" + row +"]["+ col + "] = " + classroom[row][col]);
            }
        }

        //for each loop with multi dimensional array
        for(String[] row: classroom){
            for(String column: row){
                System.out.println(column);
            }
        }

        //print as matrix
        for(int row=0;row<classroom.length;row++){
            for(int column=0;column<classroom.length;column++){
                System.out.print(classroom[row][column] + " ");
            }
            System.out.println();
        }


        //Mini activity
        String[][] b285Matrix = {{"*","*","*"},{"2","8","5"},{"a","b","c"}};

        for(int i=0;i<b285Matrix.length;i++){
            for(int j=0;j<b285Matrix.length;j++){
                System.out.print(b285Matrix[i][j]);
            }
            System.out.println();
        }


        //for-each with Arraylist
        //Syntax
        /*
        * arrayListName.foreach(Consumer<E> -> //code block);
        * "->" lambda operator = used to separate parameter and implementation
        *
        * */

        ArrayList<Integer> numbers = new ArrayList<>();

        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);
        System.out.println("Arraylist: " + numbers);


        numbers.forEach(num -> System.out.println("Arraylist: " + num));

        //forEach with HashMaps
        /*
        *
        * Syntax
        * hashMap
        *
        * */

        HashMap<String, Integer> grades = new HashMap<String, Integer>(){
            {
                this.put("English",90);
                this.put("Math",95);
                this.put("Science",97);
                this.put("History",94);
            }
        };
        grades.forEach((subject,grade)->{
          System.out.println("Hashmaps: " + subject + " : " + grade + "\n");
        });
    }
}
