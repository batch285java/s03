package com.zuitt.example;

import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args){
        //Exceptions

        //problems that arises during the execution of a program
        //it disrupts the normal flow of the program and terminates it abnormally

        //Exceptional Handling
        //refers to managing and catching run-time errors in order to safely run your code

        //Errors encountered in Java
            //compile time errors - errors that usually happens when you try to compile the program that is syntactically incorrect or has missing package imports

        //Run time error
            //errors that happens after the compilation and during the execution program
            //Common example is when a user givers a String instead of an integer

        Scanner input = new Scanner(System.in);

        int num = 0;

        System.out.println("Please enter a number");

        //try
        //try to execute the statement


        try{
          num = input.nextInt();
        }
        //catch any errors
        catch(Exception e){
            System.out.println("Invalid input");
            e.printStackTrace();
        }
        //finally particular useful when with dealing with highly sensitive operations
        finally {
            System.out.println("run no matter what");
        }
        System.out.println("Hello");
    }
}
